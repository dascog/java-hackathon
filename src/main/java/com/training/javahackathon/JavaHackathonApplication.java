package com.training.javahackathon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaHackathonApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaHackathonApplication.class, args);
    }

}
