package com.training.javahackathon.service;

import com.training.javahackathon.entity.Trade;
import com.training.javahackathon.repository.TradesRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradesService {

    @Autowired
    TradesRepository tradesRepository;

    public List<Trade> findAll() {
        return tradesRepository.findAll();
    }

    public Trade findById(long id) {
        return tradesRepository.getById(id);
    }

    // upsert function (update and insert)
    public Trade save(Trade trade) {
        return tradesRepository.save(trade);
    }

    public void deleteById(long id) {
        tradesRepository.deleteById(id);
    }


}
