package com.training.javahackathon.repository;

import com.training.javahackathon.entity.Trade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TradesRepository extends JpaRepository<Trade, Long> {
}
