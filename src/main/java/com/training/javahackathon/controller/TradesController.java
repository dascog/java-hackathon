package com.training.javahackathon.controller;

import com.training.javahackathon.entity.Trade;
import com.training.javahackathon.service.TradesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/v1/trades")
public class TradesController {

    @Autowired
    TradesService tradesService;

    @GetMapping
    public List<Trade> findAll() {
        return tradesService.findAll();
    }
    @GetMapping("/{id}")
    public ResponseEntity<Trade> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<Trade>(tradesService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Trade> create(@RequestBody Trade trade) {
        try {
            return new ResponseEntity<Trade>(tradesService.save(trade),HttpStatus.CREATED);
        } catch ( Exception e ) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable long id) {
        try {
            tradesService.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }
}