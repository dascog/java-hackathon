package com.training.javahackathon.entity;

import javax.persistence.*;

@Entity(name="Trades")
public class Trade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @Column(name = "StockTicker", nullable = false, length = 5)
    private String stockTicker;

    @Column(name = "Price", nullable = false)
    private double price;

    @Column(name = "Volume", nullable = false)
    private int volume;

    @Column(name = "Type", nullable = false, length = 10)
    private String buyOrSell;

    @Column(name = "StatusCode", nullable = false)
    private int statusCode;

    public void setId(Long id) {
        this.id = id;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public void setBuyOrSell(String buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getBuyOrSell() {
        return buyOrSell;
    }

    public int getVolume() {
        return volume;
    }

    public double getPrice() {
        return price;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "id=" + id +
                ", stockTicker='" + stockTicker + '\'' +
                ", price=" + price +
                ", volume=" + volume +
                ", buyOrSell='" + buyOrSell + '\'' +
                ", statusCode=" + statusCode +
                '}';
    }
}
