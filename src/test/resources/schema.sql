CREATE SCHEMA TradesDB;

USE TradesDB;

CREATE TABLE IF NOT EXISTS Trades
(ID INT NOT NULL AUTO_INCREMENT,
 StockTicker VARCHAR(6) NOT NULL,
    Price DOUBLE not null,
    Volume INT not null,
    Type VARCHAR(10),
    StatusCode INT not null,
    PRIMARY KEY(ID));

